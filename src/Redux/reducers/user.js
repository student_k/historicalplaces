const initialState = localStorage.user ? JSON.parse(localStorage.getItem("user")) : { "auth": false }

export default (state = initialState, payload) => {

    switch (payload.type) {
        case 'USER:SET_ME':
            return {
                token: payload.payload.token,
                user: payload.payload.user,
                auth: true
            };
        case 'USER:UNSET':
            return {
                ...payload.payload,
                auth: false
            };
        default:
            return state
    }
}
