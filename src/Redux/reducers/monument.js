const initialState = [{}]

export default (state = initialState, payload) => {

    switch (payload.type) {
        case 'MONUMENT:SET_ALL':
            return {
                items: payload.payload
            };
        default:
            return state
    }
}
