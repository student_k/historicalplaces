import {combineReducers} from 'redux';
import user from './user';
import monument from './monument';
import tour from './tour';

export default combineReducers({
    user,
    monument,
    tour
}, {});
