const initialState = [];

export default (state = initialState, payload) => {
  switch (payload.type) {
    case "TOUR:SET":
      return payload.payload;
    default:
      return state;
  }
};
