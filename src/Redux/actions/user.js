import { user } from "../../api";

const Actions = {
  setUser: (items) => ({
    type: "USER:SET_ME",
    payload: items,
  }),
  unSetUser: () => ({
    type: "USER:UNSET",
    payload: {},
  }),
  getUserToken: (data) => (dispatch) => {
    dispatch(Actions.setUser(data));
    localStorage.setItem("user", JSON.stringify({ ...data, auth: true }));
  },
};

export default Actions;
