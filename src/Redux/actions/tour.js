import { tour } from "../../api";

const Actions = {
  setTour: (items) => ({
    type: "TOUR:SET",
    payload: items,
  }),
  getTour: () => (dispatch) => {
    tour.getTours().then(({data}) => {
      console.log('data', data)
      dispatch(Actions.setTour(data));
    });
  },
};

export default Actions;
