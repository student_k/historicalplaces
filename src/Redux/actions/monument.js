import { monument } from "../../api";

const Actions = {
  setMonument: (items) => ({
    type: "MONUMENT:SET_ALL",
    payload: items,
  }),
  getMonument: () => (dispatch) => {
    monument.getMonuments().then(({data}) => {
      dispatch(Actions.setMonument(data));
    });
  },
  createMonument: (data) => (dispatch) => {
    monument.monumentCreate(data).then(() => {
      dispatch(Actions.getMonument());
    })
  },
  addComment: (obj, id) => (dispatch) => {
    
        return monument.addComments(obj, id)
    
  },
};

export default Actions;
