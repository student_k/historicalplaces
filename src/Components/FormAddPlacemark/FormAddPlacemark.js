import React, { useState, useEffect } from "react";
import { Form, Input, Col, Row, Button, Upload, notification  } from "antd";
import { CheckCircleOutlined } from '@ant-design/icons';
  
import { connect } from "react-redux";
import { monumentActions } from "../../Redux/actions";


import "./FormAddPlacemark.scss";
function FormAddPlacemark(props) {
  const InputGroup = Input.Group;
  const formRef = React.createRef();

  const [newMonumentCoordinats, setNewMonumentCoordinat] = useState({
    x: 46.05714,
    y: 37.915027,
  });

  const openNotification = () => {
    notification.open({
      message: 'Добавлен',
      description:
        'Новый памятник добавлен',
      icon: <CheckCircleOutlined style={{ color: '#10ea2bd9' }} />,
    });
  };

  const CONST_OBJ = {
    title: "test3",
    name: "name3",
    disc: "ddd333",
    photo: ["https://www.interfax.ru/ftproot/photos/photostory/2019/07/09/week4_700.jpg"],
    geometry: [45.35714, 37.815027],
    comments: []
}


  const [newMonument, setNewMonument] = useState(CONST_OBJ);

  useEffect(() => {
    setNewMonument({
      ...newMonument,
      geometry: [newMonumentCoordinats.x, newMonumentCoordinats.y],
    });
  }, [newMonumentCoordinats]);
  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const onReset = () => {
    formRef.current.resetFields();
  };


  const props1 = {
    action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
    listType: "picture",
    defaultFileList: [],
    className: "upload-list-inline",
  };

  return (
    <>
      <div>
        <Form ref={formRef} name="basic" initialValues={{ remember: true }}>
          <Form.Item
            label="Название"
            name="name"
            rules={[{ required: true, message: "Введите название памятника" }]}
          >
            <Input
              onChange={(e) => {
                setNewMonument({
                  ...newMonument,
                  name: e.target.value,
                });
              }}
            />
          </Form.Item>

          <Form.Item
            label="Опишите памятник"
            name="discription"
            rules={[{ required: true, message: "Введите описание памятника" }]}
          >
            <Input
              onChange={(e) => {
                setNewMonument({
                  ...newMonument,
                  disc: e.target.value,
                });
              }}
            />
          </Form.Item>

          <Form.Item
            name="upload"
            label="Добавте фото памятника"
            valuePropName="fileList"
            getValueFromEvent={normFile}
            rules={[{ required: true, message: "Добавте фото памятника" }]}
          >
            <Upload
              {...props1}
              onChange={(e) => {
                console.log(e);
                e.file && console.log(e.file.thumbUrl);
                e.file.response &&
                  setNewMonument({
                    ...newMonument,
                    photo: [...newMonument.photo, e.file.response.thumbUrl],
                  });
              }}
            >
              <Button>Выбрать фото</Button>
            </Upload>
          </Form.Item>

          <Form.Item
            label="Введите координаты"
            name="discription"
            rules={[
              { required: true, message: "Введите координаты памятника" },
            ]}
          >
            <InputGroup size="large">
              <Row gutter={8}>
                <Col span={8}>
                  <Input
                    defaultValue={newMonumentCoordinats.y}
                    onChange={(e) => {
                      setNewMonumentCoordinat({
                        ...newMonumentCoordinats,
                        y: +e.target.value,
                      });
                    }}
                  />
                </Col>
                <Col span={8}>
                  <Input
                    defaultValue={newMonumentCoordinats.x}
                    onChange={(e) => {
                      setNewMonumentCoordinat({
                        ...newMonumentCoordinats,
                        x: +e.target.value,
                      });
                    }}
                  />
                </Col>
              </Row>
            </InputGroup>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Отмена
            </Button>
            <Button
              className={"btn_add"}
              type="primary"
              onClick={() => {
                // props.addnewMonument(newMonument);
                props.createMonument(newMonument)
                console.log(newMonument)
                props.handleCancel();
                openNotification();
                setNewMonument(CONST_OBJ)
                onReset(CONST_OBJ);
              }}
            >
              Добавить памятник
            </Button>
          </Form.Item>
        </Form>
      </div>
    </>
  );
}

export default connect(
  ({ monument }) => ({
    monument: monument,
  }),
  {
    ...monumentActions,
  }
)(FormAddPlacemark);