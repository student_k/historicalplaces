import React, { useState, useEffect } from "react";
import "./MapView.scss";
import { Carousel, Button } from "antd";
import { Input } from "antd";
import { connect } from "react-redux";
import { monumentActions } from "../../Redux/actions";

const contentStyle = {
  height: "190px",
  color: "#fff",
  lineHeight: "190px",
  textAlign: "center",
};

function MapView(props) {
  const [letA, setLetA] = useState("");
  const [comments, setComments] = useState([]);

  useEffect(() => {
    setComments(props.currentPlacemarks.comments);
  }, [props.currentPlacemarks.comments])

  useEffect(() => {
    console.log('comments', comments)
  }, [comments])

  return (
    <div className={"map-view"}>
      <div className="map-view__left">
        <div className="monument__name">{props.currentPlacemarks.name}</div>
        <div>{props.currentPlacemarks.disc}</div>
        <Carousel dotPosition={"right"}>
          {props.currentPlacemarks.photo &&
            props.currentPlacemarks.photo.map((item) => (
              <div key={item}>
                <div style={contentStyle}>
                  <img className={"photo"} key={item} src={item} alt="" />
                </div>
              </div>
            ))}
        </Carousel>
      </div>
      {props.currentPlacemarks.name ? (
        <div className="map-view__right">
          <h3>Диолог по памятнику</h3>
          {props.user.auth ? (
            <div>
              {comments && comments.map((comment) => (
                <div key={comment.text + Date.now()}>
                  <div className={"name"}>{comment.name}</div>
                  <div>{comment.text}</div>
                </div>
              ))}
              <Input
                placeholder="Basic usage"
                onChange={(e) => {
                  setLetA(e.target.value);
                }}
                value={letA}
              />
              <br />
              <br />
              <Button
                onClick={() => {
                  // props.addComment("comments" : [{"name" : props.user.user.name, "text" : letA}], props.currentPlacemarks._id)
                  props.addComment(
                    { comments: [{ name: props.user.user.name, text: letA }] },
                    props.currentPlacemarks._id
                  ).then((data) => {
                    setComments(data.data.comments)
                    setLetA('');
                  })
                }}
                type="primary"
              >
                Добавить отзыв
              </Button>
            </div>
          ) : (
            "Авторизуйтесь"
          )}
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export default connect(
  ({ user }) => ({
    user: user,
  }),
  {
    ...monumentActions,
  }
)(MapView);
