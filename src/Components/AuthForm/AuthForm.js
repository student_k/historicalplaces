import React, { useEffect, useState } from "react";
import { Form, Input, Button, notification } from "antd";
import { CloseCircleOutlined, CheckCircleOutlined } from "@ant-design/icons";

import { user as userApi } from "../../api";

import { connect } from "react-redux";
import { userActions } from "../../Redux/actions";

import "./AuthForm.scss";
function AuthForm(props) {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [pass, setPass] = useState("");

  const [auth, _setAuth] = useState(props.user.auth);

  const [formTarget, setFormTarget] = useState(true);

  const openNotification = (text, title, status) => {
    notification.open({
      message: title,
      description: text,
      icon: status ? (
        <CheckCircleOutlined style={{ color: "#10ea2bd9" }} />
      ) : (
        <CloseCircleOutlined style={{ color: "#ec0b0bd9" }} />
      ),
    });
  };

  useEffect(() => {
    _setAuth(props.user.auth);
  }, [props.user.auth]);

  return (
    <>
      {auth ? (
        <div>
          <div>
            email : {props.user.user.email}
            <br />
            name : {props.user.user.name}
          </div>
          <div
            className={"authForm__link"}
            onClick={() => {
              localStorage.clear();
            }}
          >
            выйти
          </div>
        </div>
      ) : (
        <div>
          {formTarget ? (
            <Form name="basic" initialValues={{ remember: true }}>
              <div className={"authForm__title"}>ВОЙТИ</div>

              <Form.Item
                label="Почта"
                name="email"
                rules={[{ required: true, message: "Введите email" }]}
              >
                <Input
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
              </Form.Item>

              <Form.Item
                label="Пароль"
                name="pass"
                rules={[{ required: true, message: "Введите пароль" }]}
              >
                <Input
                  type="password"
                  onChange={(e) => {
                    setPass(e.target.value);
                  }}
                />
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  onClick={() => {
                    userApi
                      .getUserToken({
                        email: email,
                        password: pass,
                      })
                      .then(({ data }) => {
                        if (data.status === "success") {
                          openNotification(
                            "Вы вошли в свой аккаунт",
                            "Вход",
                            true
                          );
                          props.getUserToken(data);
                        } else {
                          openNotification(
                            "Неправильный пароль или почта",
                            "Ошибка",
                            false
                          );
                        }
                      });
                  }}
                >
                  ВОЙТИ
                </Button>
              </Form.Item>

              <div
                className={"authForm__link"}
                onClick={() => {
                  setFormTarget(false);
                }}
              >
                авторизация
              </div>
            </Form>
          ) : (
            <Form name="basic" initialValues={{ remember: true }}>
              <div className={"authForm__title"}>АВТОРИЗОВАТЬСЯ</div>

              <Form.Item
                label="Почта"
                name="email"
                rules={[{ required: true, message: "Введите email" }]}
              >
                <Input
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
              </Form.Item>

              <Form.Item
                label="Имя"
                name="name"
                rules={[{ required: true, message: "Введите имя" }]}
              >
                <Input
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                />
              </Form.Item>

              <Form.Item
                label="Пароль"
                name="pass"
                rules={[{ required: true, message: "Введите пароль" }]}
              >
                <Input
                  type="password"
                  onChange={(e) => {
                    setPass(e.target.value);
                  }}
                />
              </Form.Item>

              <Form.Item
                label="Повторите пароль"
                name="pass1"
                rules={[{ required: true, message: "Введите пароль" }]}
              >
                <Input type="password" onChange={() => {}} />
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  onClick={() => {
                    userApi
                      .userSignUp({
                        email: email,
                        name: name,
                        password: pass,
                      })
                      .then((data) => {
                        openNotification("Аккаунт создан", "Вход", true);
                        let localData = {
                          email: data.data.message.email,
                          pass: data.data.message.password,
                        };

                        userApi
                          .getUserToken({
                            email: localData.email,
                            password: localData.pass,
                          })
                          .then(({ data }) => {
                            if (data.status === "success") {
                              props.getUserToken(data);
                            }
                          });
                      })
                      .catch(() => {
                        openNotification(
                          "Такой пользователь уже сущетсвует",
                          "Ошибка",
                          false
                        );
                      });
                  }}
                >
                  Авторизоваться
                </Button>
              </Form.Item>
              <div
                className={"authForm__link"}
                onClick={() => {
                  setFormTarget(true);
                }}
              >
                войти
              </div>
            </Form>
          )}
        </div>
      )}
    </>
  );
}

export default connect(
  ({ user }) => ({
    user: user,
  }),
  {
    ...userActions,
  }
)(AuthForm);
