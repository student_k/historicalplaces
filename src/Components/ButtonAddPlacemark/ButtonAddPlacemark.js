import React, { useState } from "react";
import { Button, Modal } from "antd";
import { FormAddPlacemark } from "../FormAddPlacemark";

import "./ButtonAddPlacemark.scss";
function ButtonAddPlacemark({ monuments, setMonuments }) {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const addnewMonument = (newMonument) => {
    setMonuments([...monuments, newMonument]);
  }

  return (
    <>
      <Button
        onClick={() => {
          showModal();
        }}
        type="primary"
        style={{marginLeft: '10px'}}
      >
        Добавить Памятник
      </Button>
      <Modal
        title="Добавление нового Памятника"
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={[]}
      >
        <FormAddPlacemark handleCancel={handleCancel} addnewMonument={addnewMonument}/>
      </Modal>
    </>
  );
}

export default ButtonAddPlacemark;
