import React, { useState, useEffect } from "react";
import { Button, Modal, Divider, Space, Radio } from "antd";
import { Checkbox } from "antd";

import { tour } from "../../api";

import { connect } from "react-redux";
import { tourActions } from "../../Redux/actions";

function Tours(props) {
  useEffect(() => {
    props.getTour();
  }, []);

  const [value, setValue] = React.useState(1);

  const onChange = (e) => {
    console.log("radio checked", e.target.value);
    setValue(e.target.value);
  };

  useEffect(() => {
    props.getTour();
  }, []);
  let counet = 0;

  return (
    <Radio.Group onChange={onChange} value={value}>
      <Space direction="vertical">
        {props.tour &&
          props.tour.map((item) => (
            <div>
              <Radio value={counet++}>
              {item.name} ({item.price} Ꝑ)
            </Radio>
            </div>
            
          ))}
        <Button
          onClick={() => {
            props.handleCancel();
          }}
          style={{ marginLeft: "40%" }}
          type="primary"
        >
          Записаться
        </Button>
      </Space>
    </Radio.Group>
  );
}

export default connect(
  ({ tour }) => ({
    tour: tour,
  }),
  {
    ...tourActions,
  }
)(Tours);
