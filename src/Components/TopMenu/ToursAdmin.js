import React, { useState, useEffect } from "react";
import { Button, Modal, Divider, Space, Radio } from "antd";
import { Checkbox } from "antd";

import { tour } from "../../api";

import { connect } from "react-redux";
import { tourActions } from "../../Redux/actions";

function ToursAdmin(props) {
  const [value, setValue] = React.useState(1);
  const onChange = (e) => {
    console.log("radio checked", e.target.value);
    setValue(e.target.value);
  };

  useEffect(() => {
    props.getTour();
  }, []);

  function onChange2(e) {
    console.log(`checked = ${e.target.checked}`);
  }

  console.log(props);
  return (
    <Space direction="vertical">
      {props.tour &&
        props.tour.map((item) => (
          <React.Fragment>
            <h4>
              {item.name} ({item.price} Ꝑ)
            </h4>
            <div>
              {item.members.map((member) => (
                <div>
                  <Checkbox onChange={onChange2}>
                    <div>
                      <div>{member.name}</div>
                      <div>{member.phone}</div>
                    </div>
                    <br />
                  </Checkbox>
                </div>
              ))}
            </div>
            <Divider />
          </React.Fragment>
        ))}
    </Space>
  );
}

export default connect(
  ({ tour }) => ({
    tour: tour,
  }),
  {
    ...tourActions,
  }
)(ToursAdmin);
