import React, { useState } from "react";
import "antd/dist/antd.css";
import { ButtonAddPlacemark } from "../ButtonAddPlacemark";
import { ButtonAuth } from "../ButtonAuth";
import { Button, Modal, Divider, Space, Radio } from "antd";
import { connect } from "react-redux";

import ToursAdmin from "./ToursAdmin";
import Tours from "./Tours";

import "./TopMenu.scss";

function TopMenu({ monuments, setMonuments, user }) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalVisibleAdmin, setIsModalVisibleAdmin] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const showModalAdmin = () => {
    setIsModalVisibleAdmin(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleCancelAdmin = () => {
    setIsModalVisibleAdmin(false);
  };

  const [value, setValue] = React.useState(1);

  const onChange = (e) => {
    console.log("radio checked", e.target.value);
    setValue(e.target.value);
  };

  return (
    <div className={"topMenu"}>
      <ButtonAuth />
      <ButtonAddPlacemark monuments={monuments} setMonuments={setMonuments} />
      {user && user.user && user.user.role === "admin" ? (
        <Button
          onClick={() => {
            showModalAdmin();
          }}
          style={{ marginLeft: "10px" }}
          type="primary"
        >
          Администрация туров
        </Button>
      ) : (
        <Button
          onClick={() => {
            showModal();
          }}
          style={{ marginLeft: "10px" }}
          type="primary"
        >
          Просмотреть туры
        </Button>
      )}

      <Modal
        title="Доступные туры"
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={[]}
      >
        <Tours handleCancel={handleCancel}/>
      </Modal>

      <Modal
        title="Администрирование туров"
        visible={isModalVisibleAdmin}
        onCancel={handleCancelAdmin}
        footer={[]}
      >
        <ToursAdmin />
      </Modal>
    </div>
  );
}

export default connect(
  ({ user }) => ({
    user: user,
  }),
  {}
)(TopMenu);
