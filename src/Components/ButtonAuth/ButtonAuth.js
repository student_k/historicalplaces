import React, { useState } from "react";
import { Button, Modal } from "antd";
import { AuthForm } from "../AuthForm";

import "./ButtonAuth.scss";
function ButtonAuth() {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const addnewMonument = (newMonument) => {
    console.log(addnewMonument);
  };

  return (
    <>
      <Button
        onClick={() => {
          showModal();
        }}
        type="primary"
      >
        Авторизоваться
      </Button>
      <Modal
        title="Аккаунт пользователя"
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={[]}
      >
        <div>
          <AuthForm />
        </div>
      </Modal>
    </>
  );
}

export default ButtonAuth;
