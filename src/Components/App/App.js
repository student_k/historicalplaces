import React, { useState, useEffect } from "react";
import { MapView } from "../MapView";
import { TopMenu } from "../TopMenu";
import "./App.scss";

import { monument } from "../../api";

import { YMaps, Map, Placemark, RouteButton } from "react-yandex-maps";
import { MapConfig } from "../../MapConfig";

import { connect } from "react-redux";
import { monumentActions } from "../../Redux/actions";

function App(props) {
  const [monuments1, setMonuments1] = useState([]);
  const [currentPlacemarks, setCurrentPlacemarks] = useState({});

  useEffect(() => {
    props.getMonument();
  }, []);

  useEffect(() => {
    props.monuments && setMonuments1(props.monuments);
  }, [props.monuments]);

  const modules = [
    "control.ZoomControl",
    "control.RouteButton",
    "geoObject.addon.balloon",
    "geoObject.addon.hint",
  ];

  return (
    <div id="map">
      <YMaps
        className={"ymap-local"}
        query={{
          apikey: "77bc944f-a298-4aa4-9678-f8ba3a4adec4",
        }}
      >
        <Map
          defaultState={MapConfig}
          modules={modules}
          className={"app"}
          onLoad={() => {
            console.log("onLoad");
          }}
        >
          <RouteButton options={{ float: "left" }} />
          {monuments1.map((monument, index) => (
            <Placemark
              key={index * 23}
              {...monument}
              properties={{ balloonContent: monument.name }}
              onClick={() => {
                setCurrentPlacemarks(monument);
              }}
            />
          ))}
        </Map>
      </YMaps>
      <MapView currentPlacemarks={currentPlacemarks} />
      <TopMenu setMonuments={setMonuments1} monuments={monuments1} />
    </div>
  );
}

export default connect(
  ({ monument }) => ({
    monuments: monument.items,
  }),
  {
    ...monumentActions,
  }
)(App);
