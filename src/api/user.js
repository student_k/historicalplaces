import axios from "./axios";

export default {
  getUserToken: (userPassAndEmailObj) => {
    return axios.post("user/login/", userPassAndEmailObj);
  },
  userSignUp: (userPassAndEmailObj) =>
    axios.post("/user/create", userPassAndEmailObj),
  userSignIn: (id) =>
    axios.post(`/user/me/${id}`),
};
