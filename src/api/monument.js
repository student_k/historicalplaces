import axios from "./axios";

export default {
  getMonuments: () => {
    return axios.get("/monuments");
  },
  addComments: (obj, id) => {
    return axios.post(`/monument/addcomment/${id}`, obj);
  },
  monumentCreate: (data) => {
    return axios.post("/monument/create", data);
  }
};
