import axios from "./axios";

export default {
  getTours: () => {
    return axios.get("/tour/all");
  },
};
